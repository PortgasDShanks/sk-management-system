<?php

/**
 * --------------------------------------------------------------------------
 * Routes
 * --------------------------------------------------------------------------
 * 
 * Here is where you can register routes for your application.
 * Now create something great!
 * 
 */

use App\Core\Routing\Route;

Route::get('/', function () {
    $pageTitle = "Welcome";
    return view('/auth/login', compact('pageTitle'));
});

Route::get('/home', function () {
    redirect('/home');
});

Route::get('/home', ['WelcomeController@home', ['auth']]);
Route::post('/add-post', ['WelcomeController@store', ['auth']]);
Route::post('/uploadPostFile/{postID}', ['WelcomeController@uploadPostFile', ['auth']]);

Route::group(["prefix" => "members", "middleware" => ["auth"]], function () {
    Route::get('/', ['MemberController@index']);

    Route::post('/save-data', ['MemberController@store']);
});

Route::group(["prefix" => "chart", "middleware" => ["auth"]], function () {
    Route::get('/', ['ChartController@index']);
}); 

Route::group(["prefix" => "services", "middleware" => ["auth"]], function () {
    Route::get('/', ['ServicesController@index']);
    Route::get('/{id}', ['ServicesController@programList']);
    Route::get('/applicants/{id}', ['ServicesController@applicants']);

    Route::post('/getprogram-details', ['ServicesController@getProgramDetails']);
    Route::post('/add-services', ['ServicesController@store']);
    Route::post('/add-program', ['ServicesController@storeProgram']);
    Route::post('/update-program', ['ServicesController@updateProgram']);
}); 

Route::group(["prefix" => "applications", "middleware" => ["auth"]], function () {
    Route::get('/', ['ApplicationController@index']);
}); 

Route::group(["prefix" => "chats", "middleware" => ["auth"]], function () {
    Route::get('/{id}', ['ChatController@index']);
    
    Route::post('/status-to', ['ChatController@changeStatus']);
    Route::post('/selectedMember', ['ChatController@getSelectedMember']);
    Route::post('/getAllMessages', ['ChatController@loadChat']);
    Route::post('/send_msg', ['ChatController@sendMessage']);
    Route::post('/read-all-msg', ['ChatController@readMsg']);
}); 

Route::group(["prefix" => "calendar", "middleware" => ["auth"]], function () {
    Route::get('/', ['CalendarController@index']);

    Route::post('/add-event', ['CalendarController@store']);
    Route::post('/delete-event', ['CalendarController@delete']);
    Route::post('/resize-event', ['CalendarController@resize']);
}); 

Route::group(["prefix" => "operational", "middleware" => ["auth"]], function () {
    Route::get('/', ['OperationalmanualController@index']);

    Route::post('/uploadFile', ['OperationalmanualController@storeFile']);
    Route::post('/delete_file', ['OperationalmanualController@deleteFile']);
}); 

Route::group(["prefix" => "rules", "middleware" => ["auth"]], function () {
    Route::get('/', ['RulesandregulationController@index']);

    Route::post('/uploadFile', ['RulesandregulationController@storeFile']);
    Route::post('/delete_file', ['RulesandregulationController@deleteFile']);
}); 
