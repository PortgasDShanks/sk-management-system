<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ServicesController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Services";
        $users = DB()->selectLoop("*","users","role_id = 'M'")->get();
        $session = Auth::user('id');
        $where = (getRole(Auth::user('role_id')) == 'C')?"":"id = '$session'";
        $service_list = DB()->selectLoop("*","services", $where)->get();

        return view('/services/index', compact('pageTitle', 'users', 'service_list'));
    }

    public function programList($id)
    {
        $pageTitle = "Programs";
        $programs = DB()->selectLoop("p.program_name, s.service_name, p.program_desc, p.appliable_or_not, p.status, p.id as program_id","programs as p, services as s","p.service_id = s.id AND s.id = '$id'")->get();

        return view('/services/programs', compact('pageTitle','id', 'programs'));
    }

    public function applicants($id)
    {
        $pageTitle = "Applicants";
        $applicants = DB()->selectLoop("u.fullname, u.address, u.email, a.status, u.contact_no, u.id as user_id, a.id as application_id","users as u, applications as a","u.id = a.user_id AND a.program_id = '$id'")->get();

        return view('/services/applicants', compact('pageTitle','id'));
    }

    public function storeProgram()
    {
        $request = Request::validate('', [
            "program_name" => ['required'],
            "program_desc" => ['required']
        ]);

        $appliable = (isset($request['appliable_check']))?1:0;

        $dataToInsert = [
            "service_id" => $request['service_id'],
            "program_name" => $request['program_name'],
            "program_desc" => $request['program_desc'],
            "appliable_or_not" => $appliable,
            "status" => 0,
            "added_by" => Auth::user('id')
        ];

        $response = DB()->insert("programs", $dataToInsert);

        echo $response;
    }

    public function store()
    {
        $request = Request::validate('', [
            "service_name" => ['required'],
            "service_desc" => ['required'],
            "assignedMember" => ['required']
        ]);

        $dataToInsert = [
            "member_id" => $request['assignedMember'],
            "service_name" => $request['service_name'],
            "service_desc" => $request['service_desc'],
        ];

        $response = DB()->insert("services", $dataToInsert);

        echo $response;
    }

    public function updateProgram()
    {
        $request = Request::validate();
        $appliable = (isset($request['appliable_check_update']))?1:0;
      

        $response = DB()->update("programs", ["program_name" => "$request[program_name_update]", "program_desc" => "$request[program_desc_update]", "appliable_or_not" => $appliable], "id = '$request[program_id]'");

        echo $response;
    }

    public function getProgramDetails()
    {
        $request = Request::validate();
        $getDetails = DB()->select("*","programs","id = '$request[id]'")->get();

        $a = array();
        $a['id'] = $getDetails['id'];
        $a['name'] = $getDetails['program_name'];
        $a['desc'] = $getDetails['program_desc'];
        $a['appliable'] = $getDetails['appliable_or_not'];

        
        echo json_encode($a);


    }


}
