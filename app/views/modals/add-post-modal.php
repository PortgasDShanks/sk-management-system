<?php
use App\Core\Request;

?>
<div class="modal fade" id="add_post_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class='modal-header'>
                <h4><span class='fas fa-edit'></span> &mdash; Add Files or Images here &mdash;</h4>
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <div class="content">
                        <div class="form-group d-flex flex-column file-bin" id="file_bin" style="position: relative;max-width: 30rem">
                            <input type="file" id="upload_file" name="upload_file" multiple data-max-files="10">
                            <input type="hidden" id='testid' value='65'>
                            <div class="row">
                                <div class="col-12 mt-3">
                                    <div class="d-flex flex-row justify-content-end">
                                        <button type="button" onclick="donePosting()" class="btn btn-secondary mr-1"> <span class='fas fa-check-circle'></span> DONE </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready( function(){
    $("#file_bin").css({
        "cssText": "display: block !important"
    });

    

});


   
</script>