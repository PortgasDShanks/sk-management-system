<form method="POST" id='updateEvents'>
    <div class="modal fade" id="showEvent" tabindex="-1" role="dialog" aria-labelledby="showEventLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showEventLabel"><span class='feather icon-edit'></span> View / Update Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Event Name</span>
                                <input type="text" class="form-control" name='show_eventName' id='show_eventName'>
                                <input type="hidden" name='eventID' id='eventID'>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Event Description</span>
                                <textarea name="show_desc" id='show_desc' class='form-control' style='resize: none' rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="update_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>