<?php
use App\Core\Auth;

function getRole($role_id)
{
    $role = DB()->select("*", "users", "role_id = '$role_id'")->get();
    return $role['role_id'];
}

function getNewMessagesNotif()
{
    $me = Auth::user('id');

    $chats = DB()->selectLoop("*", "chats as c, users as u", "c.sender_id = u.id AND receiver_id = '$me' AND status = 0 GROUP BY sender_id ORDER BY chat_datetime DESC LIMIT 7")->get();
    $content = "";
    if(count($chats) > 0){
        foreach ($chats as $chat) {
            $content .= '<a href="'.route("/chats", $chat['sender_id']).'" class="list-group-item list-group-item-action">';
                $content .= '<div class="row align-items-center">';
                    $content .= '<div class="col-auto">';
                        $content .= '<img alt="Image placeholder" src="'.public_url('/storage/images/avatar.png').'" class="avatar rounded-circle">';
                    $content .= '</div>';
                $content .= '<div class="col ml--2">';
                    $content .= '<div class="d-flex justify-content-between align-items-center">';
                    $content .= '<div>';
                        $content .= '<h4 class="mb-0 text-sm">'.$chat['fullname'].'</h4>';
                    $content .= '</div>';
                    $content .= '<div class="text-right text-muted">';
                        $content .= '<small>'.date("M d H:iA", strtotime($chat["chat_datetime"])).'</small>';
                     $content .= '</div>';
                     $content .= '</div>';
                     $content .= '<p class="text-sm mb-0">'.$chat["chat_content"].'</p>';
                $content .= '</div>';
                $content .= '</div>';
            $content .= '</a>';
        }
    }else{
        $content .= '<a href="#!" class="list-group-item list-group-item-action">';
            $content .= '<div class="row align-items-center">';
            $content .= '<div class="col ml--2">';
                    $content .= '<p class="text-sm mb-0">No New Messages</p>';
            $content .= '</div>';
            $content .= '</div>';
        $content .= '</a>';
    }

    return $content;
    
}

function messagesCounter()
{
    $me = Auth::user('id');

    $chats = DB()->select("count(*) as c", "chats", "receiver_id = '$me' AND status = 0 GROUP BY sender_id")->get();

    return ($chats['c'] == 0)?0:$chats['c'];
}

function checkIfImage($filetype)
{
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");

    if (in_array($filetype, $image_icons_array)) {
        $response = true;
    } else {
        $response = false;
    }

    return $response;
}

function getImageView($filetype, $path)
{
    $logo_path = public_url("/assets/sprnva/file_extension_icon/");
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
    $file_icons_array = array("XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX", "PDF");

    if (in_array($filetype, $image_icons_array)) {
        $icon = public_url("/../" . $path);
    } else {
        if (in_array($filetype, $file_icons_array)) {
            $icon = $logo_path . $filetype . '.png';
        } else {
            $icon = $logo_path . 'FILE.png';
        }
    }

    return $icon;
}

function split_words($string, $nb_caracs, $separator){
    $string = strip_tags(html_entity_decode($string));
    if( strlen($string) <= $nb_caracs ){
        $final_string = $string;
    } else {
        $final_string = "";
        $words = explode(" ", $string);
        foreach( $words as $value ){
            if( strlen($final_string . " " . $value) < $nb_caracs ){
                if( !empty($final_string) ) $final_string .= " ";
                $final_string .= $value;
            } else {
                break;
            }
        }
        $final_string .= "<p data-toggle='tooltip' data-placement='top' title='".$string."'>" . $separator . "<p/>";
    }
    return $final_string;
}

function getUserAvatar($id){
  
    $avatar = DB()->select("slug", "user_uploads", "user_id = '$id' AND file_category = 'P'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}

function getAvatar()
{
    $me = Auth::user('id');
    $avatar = DB()->select("slug", "user_uploads", "user_id = '$me' AND file_category = 'P'")->get();

    return $avatar['slug'];
}

function getFileType()
{
    $me = Auth::user('id');
    $avatar = DB()->select("filetype", "user_uploads", "user_id = '$me' AND file_category = 'P'")->get();

    return $avatar['filetype'];
}

function getFiles($announcement){
    $loop = DB()->selectLoop("*","user_uploads","announcement_id = '$announcement' AND file_category = 'N'")->get();
    if(count($loop) > 0){
        foreach($loop as $data){
            $iconSize = "width: " . $data['iconsize'] . ";";

            if (!checkIfImage($data['filetype'])) {
                $isNotImg = "display: inline-flex;flex-direction: row;word-break: break-all;";
            } else {
                $isNotImg = "";
            }

            echo '<div class="card ch-padd-hover" style="border: 1px solid rgb(0 0 0 / 26%);overflow: hidden;'.$isNotImg.'">';
                echo '<img class="card-img-top" style="'.$iconSize.'" src="'.getImageView($data['filetype'], $data['slug']).'" alt="Card image cap">';
                echo '<div class="card-body">';
                    echo '<p class="card-text card-text-small d-flex flex-column">';
                        echo $data['filename'];
                        echo '<small class="text-muted" style="display: flex;flex-direction: row;justify-content: space-between;align-items: center;justify-items: center;">';
                            echo $data['filesize'] . " MB | [{$data['filetype']}]";

                            echo '<span class="d-flex flex-row show-onhover">';
                                echo '<i class="fas fa-cog show-onhover text-muted" style="font-size: 14px;cursor: pointer;" onclick="fileOption('.$data['id'].', '.$data['slug'].', '.$data['filename'].')"></i>';
                            echo '</span>';
                        echo '</small>';
                    echo '</p>';
                echo '</div>';
            echo '</div>';
        }
    }
}