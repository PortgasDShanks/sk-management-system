<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">SK Members</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" data-toggle='modal' data-target='#addSkMember' class="btn btn-sm btn-neutral"> Add New</a>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0"> </h3>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush" id='category_table'>
              <thead class="thead-light">
                <tr style='text-align:center'>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email Address</th>
                  <th>Contact</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="list">
                  <?php
                  $count = 1;
                  foreach ($members as $member) {
                    echo "<tr style='text-align:center'>";
                      echo "<td>".$count++."</td>";
                      echo "<td>$member[fullname]</td>";
                      echo "<td>$member[email]</td>";
                      echo "<td>$member[contact_no]</td>";
                      echo "<td><button onclick='checkServicesAssigned(".$member['id'].")' class='btn btn-sm btn-danger' id='deleteBtn".$member['id']."'><span class='fas fa-trash'></span> Delete Member</button></td>";
                    echo "</tr>";
                  }
                  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    <?php include __DIR__ . '/add-member-modal.php'; ?>
    <script>
    $(document).ready( function(){
      $("#category_table").DataTable();
      $("#saveMember").on('submit', function(e){
        e.preventDefault();
        $("#create_btn").prop('disabled', true);
        $("#create_btn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        var url = base_url + "/members/save-data";
        var data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("SK Member Successfully Added!");
          }else{
            failed_query();
          }
        });
      });
    });

    function checkServicesAssigned(id){
      $("#deleteBtn"+id).prop("disabled", true);
      $("#deleteBtn"+id).html("<span class='fas fa-spin fa-spinner'></span> Deleting");
      $.post(base_url+"/members/delete-member", {
        id :id
      }, function(data){
        if(data > 0){
          delete_success("SK Member Successfully Deleted!");
        }else{
          failed_query();
        }
      });
    }
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>