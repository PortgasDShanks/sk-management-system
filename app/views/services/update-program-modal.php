<form method="POST" id='updatePrograms'>
    <div class="modal fade" id="updateProgram" tabindex="-1" role="dialog" aria-labelledby="updateProgramLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateProgramLabel"><span class='fa fa-check-circle'></span> Edit Program Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='program_name_update' id='program_name_update' type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <textarea name="program_desc_update" id="program_desc_update" rows="2" class='form-control'></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text text-info">Is this Appliable or Not ?</span>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <input type="checkbox" name='appliable_check_update' id='appliable_check'>
                                    <input type="hidden" name='program_id' id='program_id'>
                                </span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>