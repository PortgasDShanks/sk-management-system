<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Programs</a></li>
                <li class="breadcrumb-item active" aria-current="page">Applicants</li>
            </ol>
            </nav>
        </div>
        </div>
        
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0"> </h3>
            </div>
            <div class="table-responsive">
            <table class="table align-items-center table-flush" id='category_table'>
              <thead class="thead-light">
                <tr style='text-align:center'>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Email Address</th>
                  <th>Contact Number</th>
                  <th>Note</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="list">
                 
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(document).ready( function(){
        $("#category_table").DataTable();
      });

      $("#savePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/add-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Added!");
          }else{
            failed_query();
          }
        });
      });

      $("#updatePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/update-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Updated!");
          }else{
            failed_query();
          }
        });
      });

      function showmodalupdate(id){
        $.ajax({
            type: "POST",
            url: base_url + "/services/getprogram-details",
            data: {
              id: id
            },
            dataType: "json",
            success: function (data) {
             $("#program_id").val(id);
             $("#program_name_update").val(data.name);
             $("#program_desc_update").val(data.desc);
             if(data.appliable == 0){
              $("#appliable_check").prop('checked', false);
             }else{
              $("#appliable_check").prop('checked', true);
             }
            }
        })
       $("#updateProgram").modal();
      }
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>