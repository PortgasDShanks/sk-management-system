-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2021 at 11:50 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ganyak`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `user_id`, `content`, `datetime`) VALUES
(40, 1, 'asasasasasasasasasasas', '2021-10-04 20:16:09'),
(41, 1, 'sadasdasdas ssss', '2021-10-04 20:17:44'),
(42, 1, 'asdfsdfsd', '2021-10-04 20:18:36'),
(43, 1, 'asdasdasdasdasd', '2021-10-04 20:20:38'),
(44, 1, 'asd', '2021-10-04 20:21:51'),
(45, 1, 'urty', '2021-10-04 20:22:53'),
(46, 1, 'fd', '2021-10-04 20:23:03'),
(47, 1, 'asdasdasdas', '2021-10-04 20:25:54'),
(48, 1, 'asdasdas', '2021-10-04 20:26:29'),
(49, 1, 'asda', '2021-10-04 20:27:41'),
(50, 1, 'asdasdas', '2021-10-04 20:34:07'),
(51, 1, 'fgg', '2021-10-04 20:35:00'),
(52, 1, '', '2021-10-04 20:35:41'),
(53, 1, 's', '2021-10-04 20:38:14'),
(54, 1, 'fffff', '2021-10-04 20:44:15'),
(55, 1, 'dffff', '2021-10-04 20:45:11'),
(56, 1, 'asdasdasd', '2021-10-04 20:49:41'),
(57, 1, 'asd', '2021-10-04 20:50:28'),
(58, 1, 'sdasdasd', '2021-10-04 20:51:20'),
(59, 1, 'fffff', '2021-10-04 20:52:05'),
(60, 1, 'fff', '2021-10-04 20:52:32'),
(61, 1, 'fffhghhh', '2021-10-04 20:55:57'),
(62, 1, 'ssssd  ssdsds', '2021-10-05 04:43:58'),
(63, 1, 'fggggg', '2021-10-05 04:44:47'),
(64, 1, 'dg', '2021-10-05 04:44:54'),
(65, 1, 'etete', '2021-10-05 04:46:17'),
(66, 1, 'ggggg', '2021-10-05 04:48:10'),
(67, 1, 'dddd', '2021-10-05 05:02:15'),
(68, 1, 'twerwerwerwer', '2021-10-06 21:59:49'),
(69, 1, 'trtertertre', '2021-10-06 22:02:56'),
(70, 1, 'test', '2021-10-06 22:06:41'),
(71, 1, 'tyrty', '2021-10-06 22:08:07'),
(72, 1, 'tretrytryr', '2021-10-06 22:11:33'),
(73, 1, 'upp[[', '2021-10-06 22:11:55'),
(74, 1, 'hjghgfnb', '2021-10-06 22:13:00'),
(75, 1, 'iuyiyui', '2021-10-06 22:13:39'),
(76, 1, 'jhkhjkhjkhj', '2021-10-06 22:18:29'),
(77, 1, 'hcvcvc', '2021-10-06 22:20:05'),
(78, 1, 'ghfhfghgfhfg', '2021-10-06 22:23:49'),
(79, 1, 'gfgf', '2021-10-06 22:30:42'),
(80, 1, 'uiyiyuiyuiuy', '2021-10-06 22:32:22'),
(81, 1, '', '2021-10-06 22:51:39'),
(82, 1, 'fdfsgdfsdfiouuiouiouioui', '2021-10-06 22:51:45'),
(83, 1, 'test', '2021-11-10 10:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0=pending, 1=accepted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `chat_content` text NOT NULL,
  `chat_datetime` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `chat_content`, `chat_datetime`, `status`) VALUES
(1, 1, 4, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 1),
(2, 4, 1, 'test', '2021-09-13 15:30:36', 1),
(3, 1, 5, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 0),
(5, 1, 4, 'asdasdasdasdas', '2021-09-13 23:23:51', 1),
(6, 1, 4, 'asdasdasdasqweqweqw', '2021-09-13 23:24:43', 1),
(7, 1, 4, 'asqweqweasdaczxcasqeqweqwdased qdws dasdas', '2021-09-13 23:24:51', 1),
(8, 1, 4, 'qweqwtqrqwreqweqweqw', '2021-09-13 23:25:21', 1),
(9, 4, 1, 'test', '2021-09-13 15:30:36', 1),
(10, 5, 1, 'test', '2021-09-13 15:30:36', 1),
(11, 1, 4, 'asdasdas', '2021-09-14 00:51:01', 1),
(12, 1, 4, 'asdasdasdas', '2021-09-14 00:51:26', 1),
(13, 1, 4, 'test', '2021-09-14 00:53:00', 1),
(14, 1, 4, 'qweqwr', '2021-09-14 00:53:41', 1),
(15, 1, 4, 'test', '2021-09-14 00:54:06', 1),
(16, 1, 4, 'yuttyutyuty', '2021-09-14 00:54:46', 1),
(17, 1, 4, 'asdasdasqweqweqw', '2021-09-14 00:57:32', 1),
(18, 1, 4, 'qqqq', '2021-09-14 01:02:36', 1),
(19, 1, 4, 'adwqaeqweqw', '2021-09-14 01:05:35', 1),
(20, 1, 4, 'qweqweqwe', '2021-09-14 01:05:41', 1),
(21, 1, 4, 'asdasdasd', '2021-09-14 01:06:40', 1),
(22, 1, 4, 'wqeqweasdasdasd', '2021-09-14 01:07:53', 1),
(23, 1, 4, 'qweqwxzczxczxc', '2021-09-14 01:08:22', 1),
(24, 1, 4, 'asdasdasdas', '2021-09-14 01:11:29', 1),
(25, 1, 4, 'asdasdasdas', '2021-09-14 01:12:10', 1),
(26, 1, 4, 'asdasdasdas', '2021-09-14 01:13:16', 1),
(27, 1, 4, 'asdasdas', '2021-09-14 01:14:09', 1),
(28, 1, 4, 'sdsdsds', '2021-09-14 01:17:37', 1),
(30, 1, 4, 'asdasdasdasd', '2021-09-14 02:10:13', 1),
(31, 1, 4, 'tqqqyyfff', '2021-09-14 02:13:14', 1),
(32, 1, 4, 'qweqweqweqw', '2021-09-14 02:13:56', 1),
(33, 1, 4, 'asdasd', '2021-09-14 02:15:37', 1),
(34, 1, 4, 'test', '2021-11-10 17:52:45', 1),
(35, 1, 4, 'teestd 2', '2021-11-10 18:05:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_description` text NOT NULL,
  `program_id` int(11) NOT NULL,
  `event_date_from` datetime NOT NULL,
  `event_date_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_description`, `program_id`, `event_date_from`, `event_date_to`, `created_by`, `created_at`) VALUES
(1, 'Test event', 'ertwerdfsd fsd fsd fsdfsd', 1, '2021-11-10 00:00:00', '2021-11-12 00:00:00', 1, '2021-11-10 20:57:19'),
(2, 'tedsaas das das das dasd asd asdas', 'werwer234234 23423 423', 2, '2021-11-15 00:00:00', '2021-11-17 00:00:00', 1, '2021-11-10 20:57:19'),
(3, 'highlight current date fullcalendar', 'highlight current date fullcalendar', 3, '2021-11-16 00:00:00', '2021-11-18 00:00:00', 1, '2021-11-10 20:57:19'),
(4, 'Moves the calendar to the current date.', 'Moves the calendar to the current date.', 1, '2021-11-17 00:00:00', '2021-11-18 00:00:00', 1, '2021-11-10 20:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_name` varchar(255) NOT NULL,
  `program_desc` text NOT NULL,
  `appliable_or_not` int(1) NOT NULL DEFAULT 1,
  `status` int(1) NOT NULL COMMENT '0=Ongoing, 1=Finished, 2=Cancelled',
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `service_id`, `program_name`, `program_desc`, `appliable_or_not`, `status`, `added_by`) VALUES
(1, 1, 'Feeding Program', 'KAdto kamu d', 1, 0, 1),
(2, 1, 'Program 2', 'Sample program', 1, 0, 1),
(3, 2, 'Program test', 'Program test', 1, 0, 1),
(4, 2, 'Program sample', 'Program sample', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `member_id`, `service_name`, `service_desc`) VALUES
(1, 4, 'service 1', 'test'),
(2, 4, 'service 2', 'test'),
(3, 6, 'Service 3', 'testsadfasdas'),
(4, 4, 'service 4', 'erwerwerwe'),
(5, 6, 'service 5', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `fullname` varchar(200) DEFAULT NULL,
  `contact_no` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` varchar(2) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `login_status` int(1) NOT NULL COMMENT '0=logged-out, 1=loggedin, 2=Away'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `contact_no`, `address`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`, `login_status`) VALUES
(1, 'ibayonabel@gmail.com', 'Abel Bayon', '', '', 'abel', '$2y$10$MzO2PrEN5UjsIVzq2fSX/uM3lklTyuB0NDGhTxNMgP67i9z9cGOiu', 'C', NULL, '2021-08-17 23:57:53', '2021-08-17 23:57:53', 1),
(4, 'dams2020@gmail.com', 'Jihyo Park', '09213807764', '', 'jihyo', '$2y$10$nmtUs9k4/itdIt75fX/uoekuwsNuphhFMjtr2RwTcaaESv3VEhrDy', 'M', NULL, NULL, '2021-09-08 06:31:06', 2),
(5, 'test@gmail.com', 'Chaeyoung Son', '09772645142', '', 'chaeyoung', '$2y$10$vMDMfuL/rthLeCtGJA2M4.m82rGe8V5AjuFsQgSLRqqINrJYKKIdS', 'M', NULL, NULL, '2021-09-08 06:45:29', 0),
(6, 'dams2020@gmail.com', 'Tzuyu Chou', '09213807765', '', 'tzuyu', '$2y$10$MzO2PrEN5UjsIVzq2fSX/uM3lklTyuB0NDGhTxNMgP67i9z9cGOiu', 'M', NULL, NULL, '2021-09-08 06:45:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_uploads`
--

CREATE TABLE `user_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `filetype` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` text NOT NULL,
  `iconsize` text NOT NULL,
  `file_category` varchar(10) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_uploads`
--

INSERT INTO `user_uploads` (`id`, `user_id`, `slug`, `filetype`, `filename`, `filesize`, `iconsize`, `file_category`, `announcement_id`, `created_at`) VALUES
(2, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 16:20:01'),
(3, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 16:20:08'),
(4, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 17:02:57'),
(7, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'R', 0, '2021-09-29 21:10:50'),
(8, 4, 'public/assets/uploads/EBRU20210930061744.xlsx', 'XLSX', 'Book1', '0.015527', '25%', 'M', 0, '2021-09-29 22:17:44'),
(9, 4, 'public/assets/uploads/WXYK20210930062542.xlsx', 'XLSX', 'Book1', '0.015527', '25%', 'R', 0, '2021-09-29 22:25:42'),
(13, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-10-01 17:38:49'),
(14, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-10-01 17:39:01'),
(15, 1, 'public/assets/uploads/3LPL20211002052646.jpg', 'JPG', 'team_5', '0.166436', '100%', 'P', 0, '2021-10-01 21:08:19'),
(18, 1, 'public/assets/uploads/NUZR20211005021652.jpg', 'JPG', 'vue', '0.004188', '100%', 'N', 0, '2021-10-04 18:16:52'),
(19, 1, 'public/assets/uploads/H9IP20211005040325.jpg', 'JPG', '5', '0.021767', '100%', 'N', 30, '2021-10-04 20:03:25'),
(20, 1, 'public/assets/uploads/9CIT20211005041144.jpg', 'JPG', '2', '0.010613', '100%', 'N', 30, '2021-10-04 20:11:44'),
(21, 1, 'public/assets/uploads/RISJ20211005042601.PNG', 'PNG', 'erdAlegria', '0.042128', '100%', 'N', 0, '2021-10-04 20:26:01'),
(22, 1, 'public/assets/uploads/3IH220211005042636.jpg', 'JPG', 'bg_starbucks', '0.06203', '100%', 'N', 0, '2021-10-04 20:26:36'),
(23, 1, 'public/assets/uploads/ZY2Z20211007062412.docx', 'DOCX', 'Acknowledgement', '0.053632', '25%', 'N', 0, '2021-10-06 22:24:12'),
(24, 1, 'public/assets/uploads/WPMR20211007063052.docx', 'DOCX', 'GRAMMARIAN_CERTIFICATE', '0.053164', '25%', 'N', 0, '2021-10-06 22:30:52'),
(25, 1, 'public/assets/uploads/WPR020211007063233.docx', 'DOCX', 'APPENDIX_D_(CURRICULUM_VITAE)', '0.496867', '25%', 'N', 80, '2021-10-06 22:32:33'),
(26, 1, 'public/assets/uploads/FTGP20211007065157.jpg', 'JPG', 'image_2', '0.08094', '100%', 'N', 82, '2021-10-06 22:51:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_uploads`
--
ALTER TABLE `user_uploads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_uploads`
--
ALTER TABLE `user_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
