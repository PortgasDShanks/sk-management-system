<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ChatController
{
    protected $pageTitle;

    public function index($id)
    {
        $pageTitle = "Messages";
        $session = Auth::user('id');
        $users = DB()->selectLoop("*","users","id != '$session'")->get();

        return view('/chats/index', compact('pageTitle',"users","id"));
    }

    public function getSelectedMember()
    {
        $request = Request::validate();
        $response['data'] = array();
        $user = DB()->select("*","users","id = '$request[id]'")->get();
        $a = array();
        $a['id'] = $user['id'];
        $a['name'] = $user['fullname'];
        $a['status'] = ($user['login_status'] == 1)?"ONLINE":(($user['login_status'] == 2)?"AWAY":"OFFLINE");

        $a['avatar'] =  (empty(getUserAvatar($user['id'])))?'<img class="media-object img-radius" src="'.public_url('/storage/images/avatar.png').'" style="height: 50px;width: 100%;border-radius: 50%;" alt="Generic placeholder image "></img>':'<img class="media-object img-radius" style="height: 50px;width: 100%;border-radius: 50%;" src="'.getImageView(getFileType(), getUserAvatar($user['id'])) .'" alt="Generic placeholder image ">';

        array_push($response['data'], $a);
        echo json_encode($response);

    }
    public function loadChat()
    {
        $request = Request::validate();
        $sender = Auth::user('id');
        if($request['receiver_id'] != 0){
            $messages = DB()->selectLoop("*", "chats", "(receiver_id = '$request[receiver_id]' AND sender_id = '$sender') OR (receiver_id = '$sender' AND sender_id = '$request[receiver_id]') ORDER BY id ASC, chat_datetime ASC")->get();
        $a = array();
        $content = "";
        if(count($messages) > 0){
            foreach ($messages as $message) {
               
                $img = (empty(getUserAvatar($request['receiver_id'])))?'<img class="media-object img-radius" src="'.public_url('/storage/images/avatar.png').'" alt="Generic placeholder image ">':'<img class="media-object img-radius" src="'.getImageView(getFileType(), getUserAvatar($request['receiver_id'])) .'" alt="Generic placeholder image ">';
               
              //  $img = '<img class="media-object img-radius" src="'.public_url('/storage/images/avatar.png').'" alt="Generic placeholder image ">';

                if($message['sender_id'] == $sender){
                    $content .= '<div class="media">
                                    <div class="media-body media-right text-right" style="padding-right: 5px;">
                                        <p class="msg-reply  bg-primary">'.$message["chat_content"].'</p>
                                        <p><i class="icofont icofont-wall-clock f-12"></i>
                                            '.date("M d", strtotime($message['chat_datetime'])).' at '.date("H:i A", strtotime($message['chat_datetime'])).'</p>
                                    </div>
                                    <div class="media-right friend-box" style="margin-top: 15px;">
                                        <a href="#" style="background-color: #01a9ac;padding: 13px;border-radius: 50%;">
                                           Me
                                        </a>
                                    </div>
                                </div>';
                }else{
                    $content .= '<div class="media">
                                    <div class="media-left friend-box">
                                        <a href="#">
                                            '.$img.'
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <p class="msg-send">'.$message["chat_content"].'</p>
                                        <p><i class="icofont icofont-wall-clock f-12"></i>
                                        '.date("M d", strtotime($message['chat_datetime'])).' at '.date("H:i A", strtotime($message['chat_datetime'])).'</p>
                                    </div>
                                </div>';
                }
            }
            
        }else{
            $content .=  '<div class="d-flex flex-column align-items-center justify-content-center">
                            <h4 class="mb-0 text-muted welcome-msg" style="font-size:30px">No Messages Found!</h4>
                        </div>';
        }
        }else{
            $content .=  '<div class="d-flex flex-column align-items-center justify-content-center">
                            <h4 class="mb-0 text-muted welcome-msg" style="font-size:30px">Select a Member to chat!</h4>
                        </div>';
        }
        

        $a["msg_content"] = $content;
        echo json_encode($a);
    }

    public function sendMessage()
    {
        $request = Request::validate();
        $sender = Auth::user('id');
        $data_to_insert = [
            "sender_id" => $sender,
            "receiver_id" => $request['receiver_id'],
            "chat_content" => $request['chatContent'],
            "chat_datetime" => date("Y-m-d H:i:s"),
            "status" => 0
        ];

        $response = DB()->insert("chats", $data_to_insert);

        echo $response;
    }

    public function changeStatus()
    {
        $request = Request::validate();
        $change_to = ($request['status'] == 1)?2:1;
        $user = Auth::user('id');

        $response = DB()->update("users", ["login_status" => $change_to], "id = '$user'");

        echo $response;
    }

    public function readMsg()
    {
        $request = Request::validate();

        $data = [
            "status" => 1
        ];

        $response = DB()->update("chats",$data,"sender_id = '$request[receiver]'");

        echo $response;
    }


}
