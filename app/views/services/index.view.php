<?php

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

$add_display = ((getRole(Auth::user('role_id')) == 'C'))?"":"display:none";
?>
<style>
textarea{
  resize: none;
}
</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Services</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right" style='<?=$add_display?>'>
            <a href="#" data-toggle='modal' data-target='#addService' class="btn btn-sm btn-neutral"> Add New</a>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0"> <?=((getRole(Auth::user('role_id')) == 'C'))?"ALL SERVICES":"ASSIGNED SERVICES";?></h3>
            </div>
            <div class='card-body' id='body-content'>
              <div class='row'>
              <?php foreach ($service_list as $service) { ?>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats" style="background: #0000001a;box-shadow: -3px 2px 6px #888888;">
                        <div class="card-header card-header-info card-header-icon" style="text-align: left;">
                            <div class="card-icon">
                                <h2><?=$service['service_name']?></h2>
                            </div>
                            <h4 class="card-title"><?=$service['service_desc']?></h4>
                        </div>
                        <div class="card-footer">
                            <div class="stats text-center">
                            <a href="<?=route('/services', $service['id'])?>">
                                <i class="fas fa-eye text-info"></i>
                                <span>See Programs</span>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>
              <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include __DIR__ . '/add-service-modal.php'; ?>
    <script>
      $("#saveServices").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/add-services";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          $("#addService").modal('hide');
            if(result > 0){
              $.confirm({
                  icon: 'fas fa-check-circle text-green',
                  title: 'Success!',
                  content: 'Service successfully added!',
                  buttons:{
                    Okay: function(){
                      $( "#body-content" ).load(window.location.href + " #body-content" );
                    }
                  }
              });
            }else{
              failed_query();
            }
        });
      })
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>