<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ApplicationController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Applications";

        return view('/applications/index', compact('pageTitle'));
    }


}
