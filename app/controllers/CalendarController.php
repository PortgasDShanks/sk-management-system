<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class CalendarController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Calendar";

        $programs = DB()->selectLoop("*", "programs")->get();

        return view('/calendar/index', compact('pageTitle', 'programs'));
    }

    public function store()
    {
        $request = Request::validate();

        $auth = Auth::user('id');
        $data = [
            "event_name" => $request['eventName'],
            "event_description" => $request['desc'],
            "program_id" => $request['programID'],
            "event_date_from" => $request['eventFrom'],
            "event_date_to" => $request['eventTo'],
            "created_by" => $auth
        ];

        $response = DB()->insert("events", $data);

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate('');

        $response = DB()->delete("events", "id = '$request[id]'");

        echo $response;
    }

    public function resize()
    {
        $request = Request::validate('');

        $new_end_date = date("Y-m-d" , strtotime("-1 day", strtotime($request['resized'])));
        $update_data = [
            "event_date_to" => $new_end_date
        ];

        $response = DB()->update('events', $update_data, "id = '$request[id]'");

        echo $response;
    }
}
