<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class MemberController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "SK Members";
        $members = DB()->selectLoop("*","users","role_id = 'M'")->get();

        return view('/members/index', compact('pageTitle', 'members'));
    }

    public function store()
    {
        $request = Request::validate('/members', [
            "first" => ['required'],
            "last" => ['required'],
            "contact_no" => ['required'],
            "email_add" => ['required']
        ]);

        $dataToInsert = [
            "email" => $request['email_add'],
            "fullname" => $request['first'].' '.$request['last'],
            "contact_no" => $request['contact_no'],
            "username" => strtolower($request['first']),
            "password" => bcrypt(strtolower($request['last'])),
            "role_id" => "M",
            "created_at" => date("Y-m-d H:i:s")
        ];

        $response = DB()->insert("users", $dataToInsert);

        echo $response;
    }


}
