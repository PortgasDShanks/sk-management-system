<form method="POST" id='saveServices'>
    <div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="addServiceLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addServiceLabel"><span class='fa fa-check-circle'></span> Add Services</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='service_name' id='service_name' placeholder="Service Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <textarea name="service_desc" id="service_desc" placeholder='Description' rows="2" class='form-control'></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <select name="assignedMember" id="assignedMember" class='form-control'>
                                    <option value="">&mdash; Please Assign a Member &mdash; </option>
                                    <?php foreach ($users as $user) { ?>
                                        <option value="<?=$user['id']?>"><?=$user['fullname']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>