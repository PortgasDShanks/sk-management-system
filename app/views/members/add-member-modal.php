<form method="POST" id='saveMember'>
    <div class="modal fade" id="addSkMember" tabindex="-1" role="dialog" aria-labelledby="addSkMemberLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addSkMemberLabel"><span class='fa fa-check-circle'></span> Add SK Member</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='first' id='first' placeholder="First Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='last' id='last' placeholder="Last Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='contact_no' id='contact_no' placeholder="Contact Number" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-tag text-red"></i></span>
                                </div>
                                <input class="form-control" name='email_add' id='email_add' placeholder="Email Address" type="email">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>